if not Auctioneer then return end

-- Create a new Auctioneer module.
local Module = Auctioneer:Module("Stats:OverTime")
local Const = Auctioneer.Const()

-- Hook our methods
function Module:Boot(hook)
	hook(Const.ScannerItemsPush, Module.ScannerItemsPush)
end

if not AuctioneerStatsOverTimeData then
	AuctioneerStatsOverTimeData = {}
end

function Module:ScannerItemsPush(items)
	local auctionKey = Auctioneer:AuctionKey()

	local d = AuctioneerStatsOverTimeData[auctionKey]
	if not d then
		d = {}
		AuctioneerStatsOverTimeData[auctionKey] = d
	end

	local timeslice = floor(Auctioneer:Timeslice())
	for k,_ in pairs(d) do
		if k < timeslice - 24*7 then
			d[k] = nil
		end
	end

	local dd = d[timeslice]
	if not dd then
		dd = {}
		dd.count = {}
		dd.price = {}
		d[timeslice] = dd
	end

	--[[
		stat = {
			id: string,
			itemKey: { itemID, itemLevel, itemSuffix, battlePetSpeciesID },
			itemInfo: { itemName, battlePetLink, quality, iconFileID, isPet, isCommodity, isEquipment },
			itemData: { itemKey, appearanceLink, totalQuantity, minPrice, containsOwnerItem },
		}
	]]

	for _, item in ipairs(items) do
		local id = item.id
		local count, price

		local existing = dd.count[id]
		local current = item.itemData.totalQuantity
		if not existing or current > existing then
			dd.count[id] = current
		end

		existing = dd.price[id]
		current = item.itemData.minPrice
		if not existing or current < existing then
			dd.price[id] = current
		end
	end
end

function Module:Stats(auctionKey, id)
	local d = AuctioneerStatsOverTimeData[auctionKey]
	if not d then
		return
	end

	local points = Auctioneer:Points{}

	local now = Auctioneer:Timeslice()

	for timeslice, dd in pairs(d) do
		local weight = nil
		local delta = now - timeslice
		if delta <= 1 then
			weight = 5
		elseif delta <= 24 then
			weight = 2
		elseif delta <= 3 then
			weight = 1.5
		end

		local price = dd.price[id]
		if price then
			local count = dd.count[id]
			points:Add(price, count, timeslice, weight)
		end
	end

	if #points == 0 then
		return
	end

	local stat = Auctioneer:Stat{
		name = "Over time",
		points = points,
	}

	return stat
end
